// WPDDisableService.cpp : main project file.

#include "stdafx.h"
#include <queue>
#include "cheader.h"
using namespace System;
using namespace System::Collections::Specialized;
using namespace System::Configuration::Install;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Management;
using namespace System::Threading;

#include "EZUSB.h"
#include "MonitorProcess.h"

void logIt(System::String^ msg)
{
	System::String^ ss = System::String::Format(L"[CALIBRATION]{0}", msg);
	System::Diagnostics::Trace::WriteLine(ss);
	Console::WriteLine(ss);
}

HANDLE hEventWaitPlugin = CreateEvent(NULL, TRUE, FALSE, _T("plug_in_and_quit"));
std::queue<CString> g_deviceInstance;

void USBEventHandler(Object^ sender, EventArrivedEventArgs^ e)
{
	if (e->NewEvent->ClassPath->ClassName == "__InstanceCreationEvent")
	{
		logIt("USB plug in time:" + DateTime::Now);
	}
	else if (e->NewEvent->ClassPath->ClassName == "__InstanceDeletionEvent")
	{
		logIt("USB plug out time:" + DateTime::Now);
	}
	for each(USBControllerDevice^ Device in EZUSB::WhoUSBControllerDevice(e))
	{//Dependent��HID\\VID_3689&PID_8762\\7&17E8BD4A&0&0000
		//logIt("\tAntecedent��" + Device->Antecedent);
		logIt("\tDependent��" + Device->Dependent);
		String^ sInstanceid = Device->Dependent->ToUpper()->Replace("\\\\", "\\");
		if (sInstanceid->IndexOf("VID_05AC&", StringComparison::CurrentCultureIgnoreCase) > 0) continue;//ignore apple devices
		using namespace Runtime::InteropServices;
		const wchar_t* lpInstanceid =
			(const wchar_t*)(Marshal::StringToHGlobalUni(sInstanceid)).ToPointer();
		g_deviceInstance.push(lpInstanceid);
		SetEvent(hEventWaitPlugin);
	}
}

int main(array<System::String ^> ^args)
{
	//DateTime^ d = gcnew DateTime(128984449371986347);
	//d = d->AddYears(1600);
	//logIt(d->ToString());
	logIt(String::Format("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));

	//MonitorProcess^ mp = gcnew MonitorProcess(gcnew EventWaitHandle(false, EventResetMode::ManualReset));
	//mp->MonitorDrvInst();

	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);

	if (_param->Parameters->ContainsKey("timeout"))
	{
		extern int g_timeout;
		g_timeout = Convert::ToInt32(_param->Parameters["timeout"]);
	}

	EventWaitHandle^ quit = gcnew EventWaitHandle(false, EventResetMode::ManualReset, "wpd_disable_service_quit_event");
	bool createNew;
	if (_param->IsParameterTrue("start-server"))
	{
		Mutex^ mutex = gcnew Mutex(true, AppDomain::CurrentDomain->FriendlyName,  createNew);
		if (createNew)
		{
			MonitorProcess^ mp = gcnew MonitorProcess(quit);
			Thread^ thread = gcnew Thread(gcnew ThreadStart(mp, &MonitorProcess::MonitorDrvInst));
			//mp->MonitorDrvInst();
			thread->Start();

			EZUSB^ ezusb = gcnew EZUSB();
			ezusb->AddUSBEventWatcher(gcnew EventArrivedEventHandler(USBEventHandler), nullptr, TimeSpan(0, 0, 1));

			while (WaitForSingleObject(hEventWaitPlugin, INFINITE) == WAIT_OBJECT_0)
			{
				if (quit->WaitOne(0)) break;
				while (!g_deviceInstance.empty())
				{
					CString sId = g_deviceInstance.front();
					g_deviceInstance.pop();
					logIt(_T("Query: %s"), sId);
					if (IsMTPDevice(sId))
					{
						DisableMTPDevice(sId);
					}
				}
			}
		}
		else
		{
		}
	}
	else if (_param->IsParameterTrue("kill-server"))
	{
		if (quit != nullptr)
		{
			quit->Set();
			SetEvent(hEventWaitPlugin);
		}
	}
	else
	{
		Console::WriteLine("-start-server");
		Console::WriteLine("-kill-server");
	}


	return 0;
}

#include "stdafx.h"
#include "cheader.h"
#include <atlstr.h>

void logIt(TCHAR* fmt, ...)
{
	va_list args;

	CString sLog;
	va_start(args, fmt);
	sLog.FormatV(fmt, args);
	va_end(args);
	CString sLogpid;
	sLogpid.Format(_T("[WPDDisable]%s"), sLog);
	_tprintf(sLog);
	OutputDebugString(sLogpid);
}

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include <Windows.h>
#include <tchar.h>
#include <atlbase.h>
#include <atlstr.h>

#include <InitGuid.h>
#include <cfgmgr32.h>
#include <setupapi.h>
#include <usbiodef.h>
#include <usbioctl.h>

#pragma once
#include "cheader.h"
using namespace System;
using namespace System::Threading;
using namespace System::Collections::Concurrent;
using namespace System::Management;
using namespace System::Collections::Generic;
using namespace System::Diagnostics;
using namespace System::Text::RegularExpressions;

ref class MonitorProcess
{
private:
	static ConcurrentDictionary<int, Int64>^ dataNew = gcnew ConcurrentDictionary<int, Int64>();

	static void startWatch_EventArrived(Object^ sender, EventArrivedEventArgs^ e)
	{
		ENTER_FUNCTION();
		int processId = Convert::ToInt32(e->NewEvent->Properties["ProcessId"]->Value->ToString());
		//int parentPID = Convert::ToInt32(e->NewEvent->Properties["ParentProcessID"]->Value->ToString());
		Int64 timecreated = Convert::ToInt64(e->NewEvent->Properties["TIME_CREATED"]->Value->ToString());
		//Console::WriteLine(String::Format("Pid={0}, parentpid={1}", processId, parentPID));
		dataNew->TryAdd(processId, timecreated);
		_recvprocess->Set();
	}

	static void stopWatch_EventArrived(Object^ sender, EventArrivedEventArgs^ e)
	{
		ENTER_FUNCTION();
		int processId = Convert::ToInt32(e->NewEvent->Properties["ProcessId"]->Value->ToString());
		//int parentPID = Convert::ToInt32(e->NewEvent->Properties["ParentProcessID"]->Value->ToString());
		//Console::WriteLine(String::Format("Pid={0}, parentpid={1}", processId, parentPID));
		//Int64 processId = Convert::ToInt64(e->NewEvent->Properties["TIME_CREATED"]->Value->ToString());
		Int64 value;
		dataNew->TryRemove(processId, value);
	}

	EventWaitHandle^ _quit;
	static EventWaitHandle^ _recvprocess;

	static bool bExit;

	static String^ GetPIDCommandLine(int processId)
	{
		//ENTER_FUNCTION();
		//DrvInst.exe "1" "0" "USB\VID_04E8&PID_6860&ADB\b&36f411a3&0&0003"                         "" "" "46ac0b427" "0000000000000000"
		//DrvInst.exe "1" "0" "USB\VID_04E8&PID_6860&MS_COMP_MTP&SAMSUNG_Android\b&36f411a3&0&0000" "" "" "4449667d3" "0000000000000000"
		String^ CLs = String::Empty;
		try
		{
			ManagementObjectSearcher^ searcher = gcnew ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + processId);
			String^ sl;
			for each(ManagementBaseObject^ obj in searcher->Get())
			{
				sl = obj["CommandLine"]->ToString();
				if (String::IsNullOrEmpty(sl)) break;
				Regex^ reg = gcnew Regex("\"(?<instance>USB\\\\VID_.*?\\\\.*?)\"", RegexOptions::IgnoreCase);
				Match^ m = reg->Match(sl);

				if (m->Success)
				{
					CLs = m->Groups["instance"]->Value;
					Trace::WriteLine(sl);
					break;
				}
			}

		}
		catch (...){}
		return CLs;
	}

	static void WaitProccessExit(Object^ pid)
	{
		try{
			Process^ p = Process::GetProcessById(Convert::ToInt32(pid));
			extern int g_timeout;
			if (!p->WaitForExit(g_timeout*1000))
			{
				logIt(_T("find process timeout and Kill :%d\n"), Convert::ToInt32(pid));
				if (!p->HasExited)
				{
					p->Kill();
				}
			}
		}
		catch (...)
		{
		}
	}

	static void ThreadProc()
	{
		ENTER_FUNCTION();
		Int64 value;
		while (!bExit)
		{
			if (dataNew->Count > 0)
			{
				array<int>^ keys = gcnew array<int>(dataNew->Keys->Count);
				dataNew->Keys->CopyTo(keys, 0);
				for each (int pid in keys)
				{
					String^ cmdl = GetPIDCommandLine(pid);
					if (!String::IsNullOrEmpty(cmdl))
					{
						String^ sID = cmdl;
						Console::Write(sID);
						using namespace Runtime::InteropServices;
						const wchar_t* lpInstanceid =
							(const wchar_t*)(Marshal::StringToHGlobalUni(sID)).ToPointer();

						if (IsMTPDevice(lpInstanceid))
						{
							logIt(_T("find MTP and Kill :%s\n"), lpInstanceid);
							dataNew->TryRemove(pid, value);
							try{
								Process^ p = Process::GetProcessById(pid);
								if(!p->HasExited)
									p->Kill();
							} catch (...)
							{ }

							continue;
						}
					}

					dataNew->TryRemove(pid, value);
					logIt(_T("start thread timeout(30s) and Kill :%d\n"), pid);
					Thread^ thread = gcnew Thread(gcnew ParameterizedThreadStart(WaitProccessExit));
					thread->Start(pid);
				}
			}
			else
			{
				extern int g_timeout;
				_recvprocess->WaitOne(g_timeout * 1000);
			}
		}
	}

public:

	MonitorProcess(EventWaitHandle^ quit)
	{
		_quit = quit;
		bExit = false;
		_recvprocess = gcnew EventWaitHandle(false, EventResetMode::AutoReset);
	}

	void MonitorDrvInst()
	{
		ENTER_FUNCTION();
		ManagementEventWatcher^ startWatch = gcnew ManagementEventWatcher(
			gcnew WqlEventQuery("SELECT * FROM Win32_ProcessStartTrace WHERE ProcessName = \"DrvInst.exe\" OR ProcessName='DsmUserTask.exe'"));
			//gcnew WqlEventQuery("SELECT * FROM Win32_ProcessStartTrace WHERE ProcessName = \"taskkill.exe\" "));
		startWatch->EventArrived += gcnew EventArrivedEventHandler(startWatch_EventArrived);
		startWatch->Start();

		ManagementEventWatcher^ stopWatch = gcnew ManagementEventWatcher(
			gcnew WqlEventQuery("SELECT * FROM Win32_ProcessStopTrace  WHERE ProcessName = \"DrvInst.exe\" OR ProcessName='DsmUserTask.exe'"));
			//gcnew WqlEventQuery("SELECT * FROM Win32_ProcessStopTrace  WHERE ProcessName = \"taskkill.exe\" "));
		stopWatch->EventArrived += gcnew EventArrivedEventHandler(stopWatch_EventArrived);
		stopWatch->Start();

		Thread^ oThread = gcnew Thread(gcnew ThreadStart(&ThreadProc));

		// Start ThreadProc.  Note that on a uniprocessor, the new 
		// thread does not get any processor time until the main thread 
		// is preempted or yields.  Uncomment the Thread::Sleep that 
		// follows oThread->Start() to see the difference.
		oThread->Start();

		_quit->WaitOne();
		bExit = true;
		_recvprocess->Set();
		startWatch->Stop();
		stopWatch->Stop();
	}

};


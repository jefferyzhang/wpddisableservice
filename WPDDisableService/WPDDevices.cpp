#include "stdafx.h"
#include "cheader.h"
#include <SetupAPI.h>
#include <devpkey.h>
#include <Devpropdef.h>
#include <map>
#include "DeviceStatus.h"

#pragma comment(lib,"Setupapi.lib")

#using <system.dll>

#define HUBINDEXCONNECTDONGLEPORT		1

using namespace System;
using namespace System::Text::RegularExpressions;

int  gStartLabel = 1;
int g_timeout = 30;


CDeviceStatus devStatus;

void PrintError(DWORD dwError, __in PCTSTR sFile, ULONG   Line)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);
	logIt(_T("Error: %s=>File:%s=>Line:%d=>ErrorCode:%d\n"), lpMsgBuf, sFile, Line, dwError);
	LocalFree(lpMsgBuf);
}

BOOL is64Bit()
{
	BOOL bIsWow64 = FALSE;
	typedef BOOL(WINAPI *LPFN_ISWOW64PROCESS)(HANDLE, PBOOL);
	LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(_T("kernel32")), "IsWow64Process");
	if (NULL != fnIsWow64Process)
	{
		if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
		{
			logIt(_T("check system 32bit or 64bit error\n"));
		}
	}
	return bIsWow64;
}

BOOL GetStringSameParent(String^ s, String^ mtpInstance)
{
	BOOL bret = FALSE;
	ENTER_FUNCTION();
	Regex^ reg = gcnew Regex("(?<sub>.*?\\\\.*?)\\\\", RegexOptions::IgnoreCase);
	Match^ m = reg->Match(s);
	if (m->Success)
	{
		if (mtpInstance->StartsWith(m->Groups["sub"]->Value, StringComparison::CurrentCultureIgnoreCase))
		{
			bret = TRUE;
		}
	}
	
	return bret;
}

BOOL IsMTPDevice(LPCTSTR sIID)
{
	ENTER_FUNCTION();
	devStatus.InitDll();
	DEVPROPTYPE type;
	BYTE b[4096] = { 0 };
	ZeroMemory(b, sizeof(b));
	int sz = sizeof(b);
	BOOL bRet = false;
	if (devStatus.procGetCommonHW(0, sIID, (PDEVPROPKEY)&DEVPKEY_Device_Parent, b, sz, type) == ERROR_SUCCESS)
	{
		logIt(_T("Parent id:%s"), (TCHAR*)b);
		if (!GetStringSameParent(gcnew String((TCHAR*)b), gcnew String(sIID))) return false;
	}

	ZeroMemory(b, sizeof(b));
	sz = sizeof(b);
	logIt(_T("get device class \n"));
	if (devStatus.procGetCommonHW(0, sIID, (PDEVPROPKEY)&DEVPKEY_Device_CompatibleIds, b, sz, type) == ERROR_SUCCESS)
	{
		TCHAR *cmpids = (TCHAR *)b;
		DWORD dwoffset = 0;
		while (cmpids != NULL && _tcslen(cmpids) > 0)
		{
			logIt(cmpids);
			if (_tcsicmp(cmpids, _T("USB\\MS_COMP_MTP")) == 0)
			{
				bRet = true;
				break;
			}
			dwoffset += _tcslen(cmpids) + 1;
			cmpids = (TCHAR *)b + dwoffset;
		}
	}
	return bRet;
}

int runExe(LPCTSTR exe, LPCTSTR args, int timeout)
{
	int ret = NO_ERROR;
	logIt(_T("runExe: ++ exe=%s, arg=%s\n"), exe, args);
	char path[MAX_PATH] = { 0 };
	if (PathFileExists(exe))
	{
		DWORD exitCode;
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		ZeroMemory(&si, sizeof(STARTUPINFO));
		si.cb = sizeof(STARTUPINFO);
		si.wShowWindow = SW_HIDE;
		TCHAR temp[1024] = { 0 };
		_stprintf_s(temp, _T("\"%s\" %s"), exe, args);
		if (CreateProcess(NULL, temp, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		{

			if (WaitForSingleObject(pi.hProcess, timeout * 1000) == WAIT_OBJECT_0)						// timeout 20 mins
			{
				GetExitCodeProcess(pi.hProcess, &exitCode);
				ret = exitCode;
			}
			else
			{
				ret = ERROR_TIMEOUT;
				// kill all process tree
			}
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}
		else
			ret = ERROR_CREATE_FAILED;
	}
	else
		ret = ERROR_FILE_NOT_FOUND;
	EXIT_FUNCTRET(ret);
	return ret;
}


void DisableMTPDevice(LPCTSTR sDevicid)
{
	TCHAR exe[MAX_PATH] = { 0 };
	ENTER_FUNCTION();
	GetModuleFileName(NULL, exe, MAX_PATH);
	PathRemoveFileSpec(exe);
	if (!is64Bit())
	{
		PathAppend(exe, _T("devcon32.exe"));
	}
	else{
		PathAppend(exe, _T("devcon64.exe"));
	}

	if (PathFileExists(exe) && _tcslen(sDevicid) > 0)
	{
		TCHAR args[MAX_PATH] = { 0 };
		_stprintf_s(args, _T("disable @\"%s\""), sDevicid);
		logIt(_T("will run devcon: %s --- %s\n"), exe, args);
		runExe(exe, args, 30);
	}
	else
		logIt(_T("devcon is not exist\n"));
	
	EXIT_FUNCTION();

}
